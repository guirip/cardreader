package fr.guirip.renamers;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * THIS BATCH:
 *  - RENAMES FILES USING THEIR LAST MODIFICATION DATE `file.lastModified()`
 * @author guillaume
 */
public class RenammerUsingLastModification {

	public static final boolean SIMULATION = true;
	
	public static final String PATH = "/media/guillaume/3C29-1160/MERGED_SYNCED/Photos/A TRIER";
	public static final String DEST = null;
//	public static final String DEST = "/media/guillaume/Guigui/Photos/Hike/2019_04_19-21 Foret dOrient/04_avril2019_Foret_d'Orient/renamed";

	public static final String SIM_SENTENCE = "Would move ";
	public static final String MOV_SENTENCE = "Moving ";

	private static final boolean RECURSIVE = false;
	
	private static final boolean USE_DATED_DIRS = false;
	private static DatedDestDirsHandler datedDirsHandler = null;
	
	public static final SimpleDateFormat dFormat = new SimpleDateFormat("yyyy_MM_dd");
	public static final SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy_MM_dd_HH'h'mm'm'");

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		redirect(new File(PATH), 1);
	}
	
	/**
	 * 
	 * @param file
	 */
	private static void redirect(File file, int dirLevel) {
		if (file.isDirectory()) {
			if (RECURSIVE || dirLevel == 1) {
				handleDirectory(file, dirLevel+1);
			}
		} else {
			handleFile(file);
		}
	}

	/**
	 * 
	 * @param child
	 */
	private static void handleDirectory(File dir, int dirLevel) {
		System.out.println("Handling directory " + dir);
		
		File[] childs = dir.listFiles();
		for (File child : childs){
			redirect(child, dirLevel);
		}
	}

	/**
	 * 
	 * @param img
	 */
	private static void handleFile(File img) {
		try {
			File fParent = new File(PATH);
			if (!fParent.exists()){
				throw new FileNotFoundException(PATH);
			}
			File[] fImages = fParent.listFiles();

			if (USE_DATED_DIRS) {
				String datedParentPath = DEST != null ? DEST : PATH;
				System.out.println("Will move files to dated directories in "+datedParentPath);
				datedDirsHandler = new DatedDestDirsHandler(datedParentPath);
			} else {
				System.out.println("Will move files to "+ DEST != null ? DEST : PATH);
			}
			
			// Parcours
			for (File fChild : fImages){
				if (fChild.isDirectory()){
					System.out.println("Ignored directory " + fChild.getName());
				} else {
					if (fChild.getName().startsWith("20")) {
						System.out.println("Ignoring file whose name starts with a date: "+fChild.getName());
						
					} else {
						// Date directory
						File destDir = getDestDir(fChild);
						
						// File destination
						File destFile = new File(destDir, dtFormat.format(fChild.lastModified()) + "_" + fChild.getName());
						
						// Move file
						if (!SIMULATION){
							fChild.renameTo(destFile);
						}
						System.out.println((SIMULATION ? SIM_SENTENCE : MOV_SENTENCE) + fChild.getName() + " to " + destFile.getPath());
					}
				}
			}
		
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	private static File getDestDir(File file) {
		if (USE_DATED_DIRS) {
			return datedDirsHandler.getDatedDir(dFormat.format(new Date(file.lastModified())));
		}
		if (DEST != null) {
			return new File(DEST);
		}
		return new File(PATH);
	}
	
}
