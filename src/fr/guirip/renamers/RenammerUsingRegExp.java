package fr.guirip.renamers;

import java.io.File;
import java.util.Arrays;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.guirip.util.SortFilesFirst;

/**
 * THIS BATCH:
 *  - APPLIES A REGEXP ON FILE NAMES TO RECONSTRUCT THE APPROPRIATE DATE PREFIX
 *  - DOES NOT CREATE ONE SUB-DIRECTORY PER DAY
 * 
 * @author guillaume
 */
public class RenammerUsingRegExp {

	public static final boolean SIMULATION = true;
	
	public static final String PATH = "/media/guillaume/3C29-1160/MERGED_SYNCED/_photos/PHOTOS-PHONE-A-TRIER/test";
	
	// if DEST = null, files are renamed in place (useful for nested directories)
	public static final String DEST = null;
	// public static final String DEST = "/media/guillaume/3C29-1160/MERGED_SYNCED/_photos/PHOTOS-PHONE-A-TRIER/2024_07_19_a_2024_09_21/renamed"; // MUST EXIST

	public static final String SIM_SENTENCE = "Would move ";
	public static final String MOV_SENTENCE = "Moving ";

	private static final boolean RECURSIVE = true;
	private static final int DECALAGE_HORAIRE = 0;
	
//	private static DatedDestDirsHandler datedDirsHandler = new DatedDestDirsHandler(DEST);

	private static final Pattern p = Pattern.compile(
//			"\\d{4}_\\d{2}_\\d{2}_\\d{2}h\\d{2}m_IMG_(\\d{4})(\\d{2})(\\d{2})_(\\d{2})(\\d{2})\\d{2}\\.jpg)");
			"[TRVIDMG]{3,4}_(\\d{4})(\\d{2})(\\d{2})_(\\d{2})(\\d{2})\\d{2}(\\d{3})?.*\\.[mp4jJpdDPnN3gGheifarwARWxcfXCF]{3,4}");
	
	/**
	 * 
	 * @param args
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		File src = new File(PATH);
		
		File destDir;
		if (DEST == null) {
			if (src.isFile()) {
				destDir = src.getParentFile();
			} else {
				destDir = src;
			}
		} else {
			destDir = new File(DEST);
		}
		
		iterate(src, true, destDir);
	}
	
	/**
	 * 
	 * @param file
	 */
	private static void iterate(File file, boolean isRoot, File destDir) {
		if (file.isDirectory()){
			if (RECURSIVE || isRoot) {
				System.out.println("\r\n > Handling directory " + file);
				
				File[] childs = file.listFiles();
				Arrays.sort(childs, new SortFilesFirst());
				for (File child : childs){
					iterate(child, false, isRoot ? destDir : new File(destDir, file.getName()));
				}
			}
		} else {
			handleFile(file, destDir);
		}
	}

	private static Calendar getCalendar(File img) {
		if (img.getName().startsWith("20")) {
			System.out.println("File seems already renamed: "+img.getName());
			return null;
		}
		
		Matcher m = p.matcher(img.getName());

		if (m.matches() != true){
			System.err.println("Cannot match " + img.getName());
			return null;			
		}

		/*
		System.out.println("Matched :\n");
		System.out.println("0 : " + m.group(0)); // Full string
		System.out.println("1 : " + m.group(1)); // year
		System.out.println("2 : " + m.group(2)); // month
		System.out.println("3 : " + m.group(3)); // day
		System.out.println("4 : " + m.group(4)); // hour
		System.out.println("5 : " + m.group(5)); // minutes
		System.out.println("---------------------------\n");
		*/

		Calendar cal = Calendar.getInstance();
	    cal.clear();
		cal.set(Calendar.YEAR, Integer.parseInt(m.group(1), 10));
		cal.set(Calendar.MONTH, Integer.parseInt(m.group(2), 10));
		cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(m.group(3), 10));
		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(m.group(4), 10)+DECALAGE_HORAIRE);
		cal.set(Calendar.MINUTE, Integer.parseInt(m.group(5), 10));

		return cal;
	}

	/**
	 * 
	 * @param img
	 */
	private static void handleFile(File img, File destDir) {
		Calendar cal = getCalendar(img);
		
		if (cal != null){

			int month = cal.get(Calendar.MONTH),
				day = cal.get(Calendar.DAY_OF_MONTH),
				hour = cal.get(Calendar.HOUR_OF_DAY),
				minutes = cal.get(Calendar.MINUTE);
			
			String dateLabel = (
					cal.get(Calendar.YEAR) + "_"
					+ (month < 10 ? "0" : "") + month + "_"
					+ (day < 10 ? "0" : "") + day
			);
			
			String hourLabel = (
				(hour < 10 ? "0" : "") + hour + "h"
				+ (minutes < 10 ? "0" : "") + minutes // + "m"
			);

			File destFile = new File(
//					datedDirsHandler.getDatedDir(dateLabel),
					destDir,
					dateLabel + "_" + hourLabel + "_" + img.getName());

			if (SIMULATION){
				System.out.println(SIM_SENTENCE + img.getName() + " to " + destFile.getPath());
			}
			else {
				if (!destFile.getParentFile().exists()) {
					boolean result = destFile.getParentFile().mkdirs();
					if (!result) {
						System.err.println(" ⚠ Failed to create some directories ("+destFile.getParent()+")");
					}
				}
				boolean result = img.renameTo(destFile);
				
				if (!result) {
					System.err.println("Failed to rename "+img.getName() + " to " + destFile.getPath());
				} else {
					System.out.println(MOV_SENTENCE + img.getName() + " to " + destFile.getPath());
				}
			}
		}
	}
	
}
