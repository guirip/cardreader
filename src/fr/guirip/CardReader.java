package fr.guirip;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import org.apache.commons.io.FileUtils;

public class CardReader extends JFrame implements ActionListener, WindowListener {

	private static final long serialVersionUID = -5828541317558097202L;
	
	public static final String DEFAULT_SOURCE_RELATIVE_PATH_TO_PHOTOS = "DCIM";
	public static final String DEFAULT_SOURCE_RELATIVE_PATH_TO_VIDEOS = "PRIVATE/AVCHD/BDMV/STREAM";
	
	public static final String DEFAULT_DESTINATION_RELATIVE_PATH_TO_PHOTOS = "";
	public static final String DEFAULT_DESTINATION_RELATIVE_PATH_TO_VIDEOS = "videos";

	public static final String LABEL_TYPE_PHOTO = "photo";
	public static final String LABEL_TYPE_VIDEO = "vidéo";
	
	public static final String FRAME_TITLE = "Récupération des photos et vidéos";
	
	public static final String FRAME_HEIGHT_PARAM_KEY = "height";
	public static final String FRAME_WIDTH_PARAM_KEY = "width";
	public static final int DEFAULT_FRAME_HEIGHT = 900;
	public static final int DEFAULT_FRAME_WIDTH = 1152;

    private final static int TOP_INSET = 15;
    private final static int SIDES_INSET = 10;
    private final static int BOTTOM_INSET = 10;
    private final static int BETWEEN_X_INSET = 4;
    private final static int BETWEEN_Y_INSET = 4;

    public static final String COLOR_GREY = "#aaaaaa";
    public static final String COLOR_ORANGE = "orange";
    public static final String COLOR_RED = "red";
    
	private static final SimpleDateFormat simpleDfDate = new SimpleDateFormat("yyyy_MM_dd");
	private static final SimpleDateFormat simpleDfDateHeure = new SimpleDateFormat("yyyy_MM_dd_HH'h'mm'm'");	
	
	private ArrayList<File[]> listeCopiesPhotos;
	private ArrayList<File[]> listeCopiesVideos; 
	private long totalSizeToCopy;
	private long totalSizeCopied;
	
	private JTextField sourceValue;
	private JButton boutonSourceChooser;

	private JTextField destinationValue;
	private JButton boutonDestinationChooser;
	
	private static final String initialButtonTitle = "Lancer la copie";
	private static final String copyingButtonTitle = "Copie en cours...";
	private JButton boutonLauncher;
	private JTextPane outputArea;
	private HTMLEditorKit outputEditor;
	private HTMLDocument outputDoc;
	private JProgressBar progressBar;

	private PropertiesManager props = null;
	
	/**
	 * Constructeur
	 */
	public CardReader(){

		super(FRAME_TITLE);

		try {
			props = new PropertiesManager();
		} catch (Exception e){
			appendAreaLogsError(e.toString());
		}
		
		// Look and Feel
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			JFrame.setDefaultLookAndFeelDecorated(true);
		}

		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
	    gbc.anchor = GridBagConstraints.BASELINE_LEADING;
		

		//  Colonne 1
		gbc.gridx = 0;
		
		JLabel labelSource = new JLabel("Source :");
		gbc.insets = new Insets(TOP_INSET, SIDES_INSET, BETWEEN_Y_INSET, BETWEEN_X_INSET);
		gbc.gridy = 0;
		gbc.gridheight = 1;
	    gbc.gridwidth = 1;
		this.add(labelSource, gbc);

		JLabel labelDestination = new JLabel("Destination :");
		gbc.insets = new Insets(BETWEEN_Y_INSET, SIDES_INSET, BETWEEN_Y_INSET, BETWEEN_X_INSET);
		gbc.gridy = 1;
	    gbc.gridwidth = 1;
		this.add(labelDestination, gbc);

		this.outputArea = new JTextPane();
		this.outputArea.setSize(300, 100);
	    this.outputArea.setEditable(false);
	    this.outputEditor = new HTMLEditorKit();
	    this.outputArea.setEditorKit(outputEditor);
	    this.outputArea.setContentType("text/html");
	    this.outputDoc = (HTMLDocument) this.outputArea.getStyledDocument();

		gbc.insets = new Insets(BETWEEN_Y_INSET, SIDES_INSET, BOTTOM_INSET, SIDES_INSET);
		gbc.gridy = 3;
	    gbc.gridwidth = 3;
	    gbc.weighty = 4;
		this.add(this.outputArea, gbc);
		JScrollPane scrollBar = new JScrollPane(this.outputArea);
		this.add(scrollBar, gbc);

		this.progressBar = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
	    gbc.weighty = 0;
		gbc.gridy = 4;
		gbc.gridheight = 1;
		gbc.weighty = 0;
		this.add(progressBar, gbc);
		
		
		// Colonne 2
		gbc.gridx = 1;
		
		this.sourceValue = new JTextField( (props != null ? props.getSource() : PropertiesManager.DEFAULT_SOURCE) );
		gbc.insets = new Insets(TOP_INSET, BETWEEN_X_INSET, BETWEEN_Y_INSET, BETWEEN_X_INSET);
		gbc.gridy = 0;
	    gbc.gridwidth = 1;
	    gbc.weightx = 0.6;
		this.add(this.sourceValue, gbc);
		gbc.weightx = 0;
		
		this.destinationValue = new JTextField( (props != null ? props.getDestination() : PropertiesManager.DEFAULT_DESTINATION) );
		gbc.insets = new Insets(BETWEEN_Y_INSET, BETWEEN_X_INSET, BETWEEN_Y_INSET, BETWEEN_X_INSET);
		gbc.gridy = 1;
		this.add(this.destinationValue, gbc);

		this.boutonLauncher = new JButton(initialButtonTitle);
		this.boutonLauncher.addActionListener(this);
		this.boutonLauncher.setMnemonic('l');
		gbc.gridy = 2;
		this.add(this.boutonLauncher, gbc);
		
		gbc.weightx = 0; 
		
		
		// colonne 3
		gbc.gridx = 2;
		
		this.boutonSourceChooser = new JButton("Choisir");
		this.boutonSourceChooser.addActionListener(this);
		gbc.insets = new Insets(TOP_INSET, BETWEEN_X_INSET, BETWEEN_Y_INSET, SIDES_INSET);
		gbc.gridy = 0;
		this.add(this.boutonSourceChooser, gbc);

		this.boutonDestinationChooser = new JButton("Choisir");
		this.boutonDestinationChooser.addActionListener(this);
		gbc.insets = new Insets(BETWEEN_Y_INSET, BETWEEN_X_INSET, BETWEEN_Y_INSET, SIDES_INSET);
		gbc.gridy = 1;
		this.add(this.boutonDestinationChooser, gbc);
		
		controleSourceWarn();
		controleDestinationWarn();
		

		// Détection de la taille de la fenêtre à afficher
		String sHeight = System.getProperty(FRAME_HEIGHT_PARAM_KEY);
		int frame_height = DEFAULT_FRAME_HEIGHT;
		if (sHeight != null){
			try {
				frame_height = Integer.parseInt(sHeight);
			} catch (Exception e){
				appendAreaLogsWarn("Valeur d'entrée du paramètre " + FRAME_HEIGHT_PARAM_KEY
						+ " incorrecte : " + sHeight 
						+ " ! Valeur par défaut utilisée : " + DEFAULT_FRAME_HEIGHT
						+ " (" + e.toString() + ")");
			}
		}
		String sWidth = System.getProperty(FRAME_WIDTH_PARAM_KEY);
		int frame_width = DEFAULT_FRAME_WIDTH;
		if (sWidth != null){
			try {
				frame_width = Integer.parseInt(sWidth);
			} catch (Exception e){
				appendAreaLogsWarn("Valeur d'entrée du paramètre " + FRAME_WIDTH_PARAM_KEY
						+ " incorrecte : " + sWidth
						+ " ! Valeur par défaut utilisée : " + DEFAULT_FRAME_WIDTH
						+ " (" + e.toString() + ")");
			}
		}
		
		this.setSize(frame_width, frame_height);
		
		this.addWindowListener(this);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	/**
	 * Vérifie le droits en lecture de la source
	 */
	private void controleSourceWarn() {
		try {
			File fSource = new File(this.sourceValue.getText());
			checkDirectory(fSource, true, false);
		} catch (Exception e){
			appendAreaLogsWarn(e.getMessage());
		}
	}
	
	/**
	 * Vérifie les droits en lecture et écriture de la destination
	 */
	private void controleDestinationWarn() {
		try {
			File fRootDestDir = new File(this.destinationValue.getText());
			checkDirectory(fRootDestDir, true, true);
		} catch (Exception e){
			appendAreaLogsWarn(e.getMessage());
		}
	}


	@Override
	public void actionPerformed(ActionEvent evt) {

		if (evt.getSource() == boutonSourceChooser) {
			String chosenPath = letUserChooseAFile(sourceValue.getText());
			if (chosenPath != null){
				sourceValue.setText(chosenPath);
				controleSourceWarn();
			}
			
		} else 	if (evt.getSource() == boutonDestinationChooser) {
			String chosenPath = letUserChooseAFile(destinationValue.getText());
			if (chosenPath != null){
				destinationValue.setText(chosenPath);
				controleDestinationWarn();
			}
			
		} else 	if (evt.getSource() == boutonLauncher) {
			boutonLauncher.setEnabled(false);
			boutonLauncher.setText(copyingButtonTitle);
			this.outputArea.setText("");

			// Lancement du traitement (Le thread permet le rafraichissement en temps réel du TextArea)
			Runnable runnable = new ValidateThread();
            Thread thread = new Thread(runnable);
            thread.start();
		}
	}

	/**
	 * 
	 * @param initialPath
	 * @return
	 */
	private String letUserChooseAFile(String initialPath){
		String returnedValue = null;
		JFileChooser fileChooser = new JFileChooser(initialPath);
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = fileChooser.showOpenDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			returnedValue = fileChooser.getSelectedFile().getPath();
		}
		return returnedValue;
	}

	/**
	 * Recherche les photos et les vidéos à copier et lance la copie vers la destination
	 */
	private void rechercheEtCopie() {
		try {
			totalSizeToCopy = 0;
			totalSizeCopied = 0;

			final String source = this.sourceValue.getText();
			final String destination = this.destinationValue.getText();

			// Sauvegarde des champs si nécessaire
			if (props != null){
				if (!props.getSource().equals(source)){
					props.setSource(source);
				}
				if (!props.getDestination().equals(destination)){
					props.setDestination(destination);
				}
			}

			// Vérification de la source
			File fSource = new File(source);
			checkDirectory(fSource, true, false);

			// Vérification de la destination
			File fRootDestDir = new File(destination);
			checkDirectory(fRootDestDir, true, true);

			listeCopiesPhotos = new ArrayList<File[]>();
			listeCopiesVideos = new ArrayList<File[]>();
			
			// Recherche des photos
			searchPictures(fRootDestDir, fSource);
			int photoCount = listeCopiesPhotos == null ? 0 : listeCopiesPhotos.size();
			String photoLabel = LABEL_TYPE_PHOTO + (photoCount > 1 ? "s" : "");
			logAmount(photoCount, photoLabel);

			// Recherche des videos
			searchVideos(fRootDestDir, fSource);
			int videoCount = listeCopiesVideos == null ? 0 : listeCopiesVideos.size();
			String videoLabel = LABEL_TYPE_VIDEO + (videoCount > 1 ? "s" : "");
			logAmount(videoCount, videoLabel);

			if (photoCount + videoCount > 0)
			appendAreaLogs("Taille totale à copier: <b>" + Util.getLibelleTaille(totalSizeToCopy) + "</b>");

			if (photoCount > 0){
				// Copie des photos
				lanceCopies(listeCopiesPhotos, photoLabel, photoCount);
			}
			if (videoCount > 0){
				// Copie des videos
				lanceCopies(listeCopiesVideos, videoLabel, videoCount);
			}
			
			appendAreaLogs("<br><br>Terminé.");

		} catch (Exception e){
			appendAreaLogsError(e.toString());
			e.printStackTrace();
		} finally {
			boutonLauncher.setEnabled(true);
			boutonLauncher.setText(initialButtonTitle);
		}
	}

	/**
	 * @param count
	 * @param label
	 */
	private void logAmount(int count, String label) {
		appendAreaLogs("<b>" + count + " " + label + " à récupérer</b><br>");
	}

	/**
	 * Scan source for pictures
	 * @param fRootDestDir
	 * @param fSource
	 * @throws Exception
	 */
	private void searchPictures(File fRootDestDir, File fSource)
			throws Exception {
		appendAreaLogs("<br>Recherche des photos...");
//		File fRootPhotos = new File(fSource, DEFAULT_SOURCE_RELATIVE_PATH_TO_PHOTOS);
		try {
			checkDirectory(fSource, true, false);
		} catch (FileNotFoundException e){
			// ignore : no photo to download, that's it.
		}

		File[] tPhotosDirs = fSource.listFiles();
		if (tPhotosDirs != null) {
			if (tPhotosDirs.length > 0){
				for (File dir : tPhotosDirs){
					parcoursRecursif(dir, fRootDestDir);
				}
			}
		}
	}

	/**
	 * Parcours récursif à la recherche des photos
	 * @param sourceDir
	 * @param parentDestDir
	 */
	private void parcoursRecursif(File sourceDir, File parentDestDir) {

		File[] tListing = sourceDir.listFiles();
		for (File fChild : tListing){
			if (fChild.getName().endsWith(".MP4")) {
				queueVideo(parentDestDir, fChild);
			} else {
				queuePhoto(parentDestDir, fChild);
			}
		}
	}

	/**
	 * Allows to reuse an existing directory instead of creating a new one.<br />
	 * e.g. "2017_01_20" would return:
	 * <ul>
	 *  <li>"2017_01_20 session" if existing</li>
	 *  <li>"2017_01_20" else</li>
	 *  </ul>
	 * @param parent
	 * @param d
	 * @return
	 */
	private String getDestDir(File parent, Date d) {
		String timestampedDirName = simpleDfDate.format(d);

		// Looks for a directory starting with the same date
		Pattern p = Pattern.compile("^"+timestampedDirName+".*");
		for (String child: parent.list()) {
			if (p.matcher(child).find()) {
				return child;
			}
		}
		return timestampedDirName;
	}
	
	/**
	 * 
	 * @param parentDestDir
	 * @param fChild
	 */
	private void queuePhoto(File parentDestDir, File fChild) {
		
		File destDir = new File(parentDestDir, getDestDir(parentDestDir, new Date(fChild.lastModified()))
						+ ( DEFAULT_DESTINATION_RELATIVE_PATH_TO_PHOTOS != null && !DEFAULT_DESTINATION_RELATIVE_PATH_TO_PHOTOS.equals("")
							? File.separator + DEFAULT_DESTINATION_RELATIVE_PATH_TO_PHOTOS
							: ""));

		// Répertoire = appel récursif
		if (fChild.isDirectory()){
			File fDest = new File(destDir, fChild.getName());
			parcoursRecursif(fChild, fDest);
			
		// Fichier = ajout à la liste
		} else {
			File fDest = new File(destDir, simpleDfDateHeure.format(new Date(fChild.lastModified())) + "_" + fChild.getName());
			if (fDest.exists()){
				appendAreaLogs("&nbsp; [ignoré] &nbsp;" + fDest + " existe déjà.", "color:"+COLOR_GREY);
			} else {
				totalSizeToCopy += fChild.length();

				// Ajout à la liste des photos à copier
				File[] tCopie = { fChild, fDest };
				listeCopiesPhotos.add(tCopie);
			}
		}
	}

	/**
	 * Scan source for videos
	 * @param fRootDestDir
	 * @param srcFile
	 * @throws Exception
	 */
	private void searchVideos(File fRootDestDir, File srcFile) throws Exception {
		appendAreaLogs("Recherche des vidéos...");
		File fRootVideos = new File(srcFile, DEFAULT_SOURCE_RELATIVE_PATH_TO_VIDEOS);
		try {
			checkDirectory(fRootVideos, true, false);
		} catch (FileNotFoundException e){
			// ignore : no photo to download, that's it.
		}

		File[] tVideosDirs = fRootVideos.listFiles();
		if (tVideosDirs != null && tVideosDirs.length > 0){
			for (File fVideo : tVideosDirs){
				queueVideo(fRootDestDir, fVideo);
			}
		}
	}

	/**
	 * 
	 * @param fRootDestDir
	 * @param srcFile
	 */
	private void queueVideo(File fRootDestDir, File srcFile) {
		// Création du répertoire de destination
		File fDestDir = new File(fRootDestDir, getDestDir(fRootDestDir, new Date(srcFile.lastModified()))
							+ ( DEFAULT_DESTINATION_RELATIVE_PATH_TO_VIDEOS != null && !DEFAULT_DESTINATION_RELATIVE_PATH_TO_VIDEOS.equals("")
								? File.separator + DEFAULT_DESTINATION_RELATIVE_PATH_TO_VIDEOS
								: ""));

		File fDest = new File(fDestDir, simpleDfDateHeure.format(new Date(srcFile.lastModified())) + "_" + srcFile.getName());
		if (fDest.exists()){
			appendAreaLogs("&nbsp; [ignoré] &nbsp;" + fDest + " existe déjà.", "color:"+COLOR_GREY);
		} else {
			totalSizeToCopy += srcFile.length();
			
			// Ajout à la liste des videos à copier
			File[] tCopie = { srcFile, fDest };
			listeCopiesVideos.add(tCopie);
		}
	}

	/**
	 * Effectue la copie des ressources
	 * @param liste
	 * @param type
	 */
	private void lanceCopies(ArrayList<File[]> liste, String label, int count){
		if (liste.size() == 0){
			appendAreaLogs("<br>Aucune " + label + " à copier.");
		} else {
			appendAreaLogs("<br><br><b>Copie des " + count + " " + label + "</b><br>");
			final Double totalSize = new Double(totalSizeToCopy);
			final DecimalFormat df = new DecimalFormat("#");

			for (File[] tCopie : liste){
				
				File fSource = tCopie[0];
				File fDest = tCopie[1];
				try {
					if (!fDest.getParentFile().exists()){
						fDest.getParentFile().mkdirs();
					}
					FileUtils.copyFile(fSource, fDest);
					totalSizeCopied += fSource.length();
					appendAreaLogs("&nbsp; [copié] &nbsp; " + fSource + " &nbsp; vers &nbsp; <b>" + fDest + "</b>");
					
					int pourcentage = Integer.valueOf( df.format( (new Double(totalSizeCopied)*100)/totalSize ) );
					this.progressBar.setValue(pourcentage);
	
				} catch (IOException e){
					appendAreaLogsError("Erreur lors de la copie de " + fSource
							+ " vers " + fDest + " (" + e.toString() + ")");
				}
			}
			
			try {			
				// code review: Why ?
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
				appendAreaLogs(e.toString());
			}
		}
	}
	
	/**
	 * Vérifie l'existence, s'il s'agit bien d'un répertoire, 
	 * et s'il est accessible en lecture
	 * @param fDir
	 * @throws Exception
	 */
	private void checkDirectory(File fDir, boolean checkRead, boolean checkWrite) throws Exception {
		
		if (!fDir.exists()){
			throw new FileNotFoundException(fDir + " n'existe pas !");
		}
		if (!fDir.isDirectory()){
			throw new FileNotFoundException(fDir + " n'est pas un répertoire !");
		}
		if (checkRead && !fDir.canRead()){
			throw new FileNotFoundException(fDir + " n'est pas accessible en lecture !");
		}
		if (checkWrite && !fDir.canWrite()){
			throw new FileNotFoundException(fDir + " n'est pas accessible en écriture !");
		}
	}

	private void appendAreaLogs(String str){
		appendAreaLogs(str, null);
	}

	private void appendAreaLogs(String str, String style){		
		try {
			String elString = "<span" + (style != null ? " style='"+style+"'" : "") + ">" + str + "</span><br/>";
			this.outputEditor.insertHTML(this.outputDoc, this.outputDoc.getLength(), elString, 0, 0, HTML.Tag.SPAN);
		} catch (Exception ex) {
			this.outputArea.setText(ex.toString());
		}
		//this.areaLogs.setCaretPosition(this.areaLogs.getText().length() - 1);
	}
	
	private void appendAreaLogsError(String str){
		appendAreaLogs("<br><b>[ERREUR] " + str + "</b><br>", "color:"+COLOR_RED);
	}
	
	private void appendAreaLogsWarn(String str){
		appendAreaLogs("<br><b>[ATTENTION] " + str + "</b><br>", "color:"+COLOR_ORANGE);
	}

	/**
	 * Permet le rafraichissement en temps réel du TextArea
	 */
	class ValidateThread implements Runnable {
		
	    public void run() {
	        rechercheEtCopie();
	    }
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		System.exit(0);
	}
	
	@Override
	public void windowClosed(WindowEvent arg0) {}
	
	@Override
	public void windowActivated(WindowEvent arg0) {}

	@Override
	public void windowDeactivated(WindowEvent arg0) {}

	@Override
	public void windowDeiconified(WindowEvent arg0) {}

	@Override
	public void windowIconified(WindowEvent arg0) {}

	@Override
	public void windowOpened(WindowEvent arg0) {}
	
}
