package fr.guirip;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertiesManager extends Properties {

	private static final long serialVersionUID = 2304988395018258199L;

	/**
	 * Repertoire contenant le fichier properties
	 */
	public static final String PATH = System.getProperty("user.home")
									+ File.separator + "." + CardReader.class.getSimpleName();
	
	/**
	 * Nom du fichier properties
	 */
	public static final String FILE = CardReader.class.getSimpleName() + ".properties";

	public static final String DEFAULT_DESTINATION = "/media/Temp/_Photos";
	public static final String DEFAULT_SOURCE = "/media/disk";
	
	private static final String PROPERTY_SOURCE = "SOURCE";
	private static final String PROPERTY_DESTINATION = "DESTINATION";
	
	private String source;
	private String destination;

	private File fFile;
	
	/**
	 * Constructeur
	 * @throws Exception
	 */
	public PropertiesManager() throws Exception {
		
		// Ouverture du fichier properties
		checkAndOpen();
		
		// Lecture des propriétés
		this.source = this.getProperty(PROPERTY_SOURCE);
		if (this.source == null){
			this.setSource(DEFAULT_SOURCE);
		}
		this.destination = this.getProperty(PROPERTY_DESTINATION);
		if (this.destination == null){
			this.setDestination(DEFAULT_DESTINATION);
		}
	}

	/**
	 * Ouvre le fichier properties
	 * @return
	 * @throws IOException
	 */
	private void checkAndOpen() throws Exception {

		// Vérifie l'existence du fichier, le crée sinon
		fFile = new File(PATH, FILE); 
		if (!fFile.exists()){
			File fDir = fFile.getParentFile();
			if (!fDir.exists()){
				fDir.mkdir();
			}
			try {
				fFile.createNewFile();
			} catch (Exception e){
				throw new Exception("Erreur lors de la creation du fichier de proprietes. "
						+ e.getClass() + " " + e.getMessage());
			}
		}
		
		// Ouvre le fichier properties
		FileReader fr = null;
		try {
			fr = new FileReader(fFile);
			this.load(fr);
		} catch (Exception e){
			throw new Exception("Erreur lors de la lecture du fichier de proprietes. "
					+ e.getClass() + " " + e.getMessage());
		} finally {
			if (fr != null){
				try { fr.close(); } catch (Exception e){ /* ignore */ }
			}
		}
	}
	
	public String getSource(){
		return source;
	}
	public String getDestination(){
		return destination;
	}
	
	public void setSource(String pSource){
		this.source = pSource;
		this.setProperty(PROPERTY_SOURCE, pSource);
		this.save();
	}
	public void setDestination(String pDestination){
		this.destination = pDestination;
		this.setProperty(PROPERTY_DESTINATION, pDestination);
		this.save();
	}
	
	/**
	 * Enregistre les modifications apportées au fichier properties
	 */
	public void save(){
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(fFile);
			this.store(fos, "");
		} catch(Exception e){
			e.printStackTrace();
		} finally {
			if (fos != null){
				try { fos.close(); } catch (Exception e){ /* ignore */ }
			}
		}
		System.out.println("Sauvegarde du fichier de proprietes effectuee");
	}
	
}
