package fr.guirip.util;

import java.io.File;
import java.util.Comparator;

public class SortFilesFirst implements Comparator<File> {
    public int compare(File a, File b){
      if (a.isFile() && !b.isFile()) {
    	  return -1;
      } else if (b.isFile()) {
    	  return 1;
      }
      return 0;
    }
}
