package fr.guirip;

import java.text.DecimalFormat;

public class Util {

	public static final DecimalFormat df = new DecimalFormat("#.##");
	public static final Double NB_OCTETS_IN_GO = new Double(1000000000);
	public static final Double NB_OCTETS_IN_MO = new Double(1000000);
	public static final Double NB_OCTETS_IN_KO = new Double(1000);
	
	/**
	 * Retourne une chaine human-readable de la taille d'un fichier
	 * @param f
	 * @return
	 */
	public static String getLibelleTaille(long length){
		Double taille = new Double(length);
		
		if (taille > NB_OCTETS_IN_GO){
			return df.format(taille/NB_OCTETS_IN_GO) + "go";
			
		} else if (taille > NB_OCTETS_IN_MO){
			return df.format(taille/NB_OCTETS_IN_MO) + "mo";
			
		} else {
			return df.format(taille/NB_OCTETS_IN_KO) + "ko";
		}
	}

}
