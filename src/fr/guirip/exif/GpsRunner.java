package fr.guirip.exif;

import java.io.File;
import java.util.Collection;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.GpsDirectory;

public class GpsRunner {

	public static final String PARAM_INPUT = "path";
	private static String path = System.getProperty(PARAM_INPUT);
	
	public static final String PARAM_SHOW_ALL_METADATA = "allMetadata";
	private static boolean showAllMetadata;

	/**
	 * main method
	 * @param args
	 */
	public static void main(String[] args){
		// Read path parameter value
		if (args.length == 0){
			String pathProperty = System.getProperty("path");
			if (pathProperty != null && pathProperty.length() > 0 && new File(path).exists()){
				args = new File(path).list();
			} else {
				throw new IllegalArgumentException("Missing parameters ! " + System.getProperty("path"));
			}
		}

		// Control only show GPS info parameter
		showAllMetadata = System.getProperty(PARAM_SHOW_ALL_METADATA) != null ? Boolean.valueOf(System.getProperty(PARAM_SHOW_ALL_METADATA)) : false;

		// Start process
		System.out.println("Starting process.");
		for (String s : args){

			// Control access to specified path
			File fInput = new File((path != null ? path + File.separator : "") + s);
			if (!fInput.exists()){
				throw new IllegalArgumentException(path + File.separator + s + " doesnt exist !");
			}
			else if (!fInput.canRead()){
				throw new IllegalArgumentException(path + File.separator + s + " is not readable !");
			}
			perform(fInput);
		}

		System.out.println("\nEnd.");
	}

	/**
	 * 
	 * @param fInput
	 */
	private static void perform(File fInput){
		if (fInput.isFile()){
			if (fInput.getName().endsWith(".jpg") || fInput.getName().endsWith(".JPG")){
				performOnFile(fInput);
			}
			else {
				System.out.println("Unmanaged file extension for file : " + fInput.getPath());
			}
		} else {
			performOnDirectory(fInput);
		}
	}

	/**
	 * 
	 * @param fDirectory
	 */
	private static void performOnDirectory(File fDirectory) {
		File[] tfChildrens = fDirectory.listFiles();
		for (int i=0, total = tfChildrens.length; i<total; i++){
			perform(tfChildrens[i]);
		}
	}

	/**
	 * 
	 * @param fImage
	 */
	private static void performOnFile(File fImage) {
		Metadata metadata = null;
		System.out.println("\n>> " + fImage.getName());

		// Read metadata
		try {
			metadata = ImageMetadataReader.readMetadata(fImage);
			if (metadata == null){
				System.err.println("No metadata found from file " + fImage.getPath());
			}
		} catch (Exception e){
			System.err.println("Error reading metadata from file " + fImage.getPath());
			e.printStackTrace();
		}

		if (metadata == null){
			System.out.println("No metadata");
		} else {
			// Generic metadatas
			if (showAllMetadata){
				for (Directory directory : metadata.getDirectories()) {
				    for (Tag tag : directory.getTags()) {
				        System.out.println(tag);
				    }
				}
			}

			// GPS metadata
			/*GpsDirectory gpsDir = metadata.getDirectory(GpsDirectory.class);
			if (gpsDir == null){
				System.out.println("No GPS data");
			} else {
				Collection<Tag> tags = gpsDir.getTags();
				for (Tag tag : tags){
					System.out.println(tag.getTagName() + "  :  " + gpsDir.getString(tag.getTagType()));
				}
				if (gpsDir.getGeoLocation() != null){
					System.out.println("lat: " + gpsDir.getGeoLocation().getLatitude() + ",  lng: " + gpsDir.getGeoLocation().getLongitude());
				} else {
					System.out.println("No latitude/longitude data");
				}
			}*/
		}
	}

}
