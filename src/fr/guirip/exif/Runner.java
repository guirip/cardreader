package fr.guirip.exif;

import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.mp4.Mp4Directory;

import fr.guirip.renamers.DatedDestDirsHandler;


/**
 * 2/10/2019
 * 
 * This batch uses:
 *  - creation date from metadata to rename photos
 *  - last modification date to rename videos
 * 
 * e.g
 *  rename/copy P1060408.JPG to 2019_07_24_09h12m06_P1060408.JPG
 *  rename/copy P1050625_cat.RW2 to 2019_07_11_12h01m54_P1050625_cat.RW2
 *  rename/copy P1050693.MP4 to 2019_07_13_09h22m40_P1050693.MP4
 * 
 * @author guillaume
 */
public class Runner {
	public static final String DEST = "/media/guillaume/3C29-1160/MERGED_SYNCED/_photos";
	
	private static final boolean SIMULATION = false;
	private static final boolean PERFORM_RSYNC = false;
	private static final boolean COPY_INSTEAD_OF_RENAME = true;
	private static final boolean LOG_EXIF = false;
	private static final boolean RENAME_VIDEOS_USING_LAST_MODIFICATION = true;
	private static final boolean CREATE_DIRECTORY_PER_DAY = true;

	private static final String LUMIX_PATH = "/media/guillaume/LUMIX";
	private static final String DISK_PATH = "/media/guillaume/disk";
	private static final String SD_PATH = "/media/guillaume/0000-0001";
	private static final String SD64_PATH = "/media/guillaume/SD64GO";
	private static final String OM_PATH = "/media/guillaume/OM SYSTEM";
	private static final String BONUS_PATH = null;
	private static final boolean RECURSIVE = true;
	
	private static final String[] inputs = {
		"/DCIM", // generic
		"/PRIVATE/AVCHD/BDMV/STREAM" // Lumix - videos
		// "/DCIM/100MSDCF", // RX10 iii - photos
		// "/MP_ROOT/100ANV01", // RX10 iii - videos
		// "/PRIVATE/M4ROOT/CLIP" // RX10 iii - videos
	};
	
	private static final String SKIP_BEFORE = "2024-10-26"; // e.g "2023-12-09"
	private static Date skipBefore;
	
	private static final String SKIP_AFTER = null; // e.g "2023-12-21"
	private static Date skipAfter;
	
	private static final boolean HANDLE_PHOTOS = true;
	private static final boolean HANDLE_VIDEOS = true;
	
	public static final String PARAM_INPUT = "input";
	public static final String PARAM_ID = "id";
	
	public static final int DECALAGE_HORAIRE_PHOTO = 0;
	public static final long DECALAGE_HORAIRE_PHOTO_IN_MS = DECALAGE_HORAIRE_PHOTO*60*60*1000;
	
	public static final int DECALAGE_HORAIRE_VIDEO = 0;
	public static final long DECALAGE_HORAIRE_VIDEO_IN_MS = DECALAGE_HORAIRE_VIDEO*60*60*1000;

	private static final String id = System.getProperty(PARAM_ID);
	private static final SimpleDateFormat destDateFormat = new SimpleDateFormat("yyyy_MM_dd");
	private static final SimpleDateFormat destHourFormat = new SimpleDateFormat("HH'h'mm'm'");

	private static final Pattern p = Pattern.compile("\\d{4}_\\d{2}_\\d{2}_\\d{2}h\\d{2}m(_.*\\.[mMpP4jJpP3gGRW2]{3})");

	private static DatedDestDirsHandler datedDirsHandler = new DatedDestDirsHandler(DEST);

	private static Pattern OUT_PP3_PATTERN = Pattern.compile("(.*).out.pp3");
	private static Pattern PP3_PATTERN = Pattern.compile("(.*).pp3");
	
	/**
	 * main method
	 * @param args
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws ParseException{
		if (SKIP_BEFORE != null) {
			Runner.skipBefore = new SimpleDateFormat("yyyy-MM-dd").parse(SKIP_BEFORE);
		}
		if (SKIP_AFTER != null) {
		    Calendar calendar = Calendar.getInstance();
	        calendar.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(SKIP_AFTER));
	        calendar.add(Calendar.DAY_OF_MONTH, 1);
			Runner.skipAfter = calendar.getTime();
		}
		
		for (int i=0; i<inputs.length; i++) {
			processInput(LUMIX_PATH+inputs[i]);
			processInput(DISK_PATH+inputs[i]);
			processInput(SD_PATH+inputs[i]);
			processInput(SD64_PATH+inputs[i]);
			processInput(OM_PATH+inputs[i]);
		}
		if (BONUS_PATH != null) {
			processInput(BONUS_PATH);
		}
		System.out.println("End.");
	}
	
	private static void processInput(String input) {
		if (input == null || input.equals("")){
			throw new IllegalArgumentException("Parameter " + PARAM_INPUT + " missing !");
		}

		File fInput = new File(input);
		if (!fInput.exists()){
			System.out.println(input + " doesnt exist");
		}
		else if (!fInput.canRead()){
			throw new IllegalArgumentException(input + " is not readable !");
		}
		else {
			System.out.println("Processing "+input);
			perform(fInput, 1);
		}
		System.out.println("");
	}

	/**
	 * 
	 * @param fInput
	 */
	private static void perform(File fInput, int dirLevel){
		if (fInput.isFile()){
			Date date = null;
			String fileName = fInput.getName();
			boolean isPhoto = false, isVideo = false;
			
			if (fileName.endsWith(".thm") || fileName.endsWith(".THM")
					|| fileName.endsWith(".lrv") || fileName.endsWith(".LRV")
					|| fileName.endsWith(".xml") || fileName.endsWith(".XML")) {
				// System.out.println("Ignoring " + fInput.getPath());
			}
			else if (HANDLE_PHOTOS && (fileName.endsWith(".jpg") || fileName.endsWith(".JPG") || fileName.endsWith(".jpeg")
					|| fileName.endsWith(".rw2") || fileName.endsWith(".RW2")
					|| fileName.endsWith(".arw") || fileName.endsWith(".ARW")
					|| fileName.endsWith(".raf") || fileName.endsWith(".RAF")
					|| fileName.endsWith(".gpr") || fileName.endsWith(".GPR")
					|| fileName.endsWith(".orf") || fileName.endsWith(".ORF")
					|| fileName.endsWith(".ori") || fileName.endsWith(".ORI"))) {
				isPhoto = true;
			}
			else if (HANDLE_VIDEOS && (fileName.endsWith(".mp4") || fileName.endsWith(".MP4")
					|| fileName.endsWith(".mts") || fileName.endsWith(".MTS")
					|| fileName.endsWith(".mov") || fileName.endsWith(".MOV")
					)){ // 3gp is not supported by the lib metadata-extractor :/
				isVideo = true;
			}
			else if (fileName.endsWith(".pp3")) {
				String baseFileName = extractPp3Filename(fileName);
				if (baseFileName != null) {
					FilenameFilter fileFilter = (dir, name) -> {
					  return !name.equals(fileName) && name.startsWith(baseFileName);
					};
					File parentDir = fInput.getParentFile();
					File[] matches = parentDir.listFiles(fileFilter);
					if (matches.length == 1) {
						date = getDateFromImage(matches[0]);
					}
				}
				if (date == null) {
					System.out.println("Failed to find corresponding source file ("+baseFileName+") for "+fileName);
				}
			}
			else {
				System.out.println("Unmanaged file extension for file : " + fInput.getPath());
			}
			if (isPhoto || isVideo) {
				if (isPhoto) {
					date = getDateFromImage(fInput);
				} else if (isVideo) {
					date = getDateFromVideo(fInput);
				}
				if (date == null) {
					System.err.println("No capture date found from file : " + fInput.getPath());
				} else {
					performOnFile(fInput, date);
				}
			}
		} else if (dirLevel == 1 || RECURSIVE) {
			performOnDirectory(fInput, dirLevel);
		}
	}

	private static String extractPp3Filename(String fileName) {
		Matcher matcher = OUT_PP3_PATTERN.matcher(fileName);
		if (matcher.matches()){
			return matcher.group(1);
		}
		matcher = PP3_PATTERN.matcher(fileName);
		if (matcher.matches()){
			return matcher.group(1);
		}
		return null;
	}

	/**
	 * 
	 * @param fDirectory
	 */
	private static void performOnDirectory(File fDirectory, int dirLevel) {
		File[] tfChildrens = fDirectory.listFiles();
		for (int i=0, total = tfChildrens.length; i<total; i++){
			perform(tfChildrens[i], dirLevel+1);
		}
	}

	/**
	 * 
	 * @param fInput
	 * @return
	 */
	private static Metadata getMetaData(File fInput) {
		Metadata metadata = null;

		// Read metadata
		try {
			metadata = ImageMetadataReader.readMetadata(fInput);
			if (metadata == null){
				System.err.println("No metadata found from file " + fInput.getPath());
			}
		} catch (Exception e){
			if (e.getMessage().equals("File format could not be determined")) {
				//System.out.println("File not supported by exif library: "+ fInput.getName());
			} else {
				System.err.println("Error reading metadata from file "+fInput.getPath()+"  #  "+e.getClass().getName()+": "+e.getMessage());
			}
		}
		return metadata;
	}

	/**
	 * 
	 * @param fInput
	 * @return
	 */
	private static Date getDateFromVideo(File fInput) {

		Date d = null;
		
		if (RENAME_VIDEOS_USING_LAST_MODIFICATION) {
			d = new Date(fInput.lastModified());
		}
		else {
			Metadata metadata = getMetaData(fInput);
			if (metadata == null) {			
				// System.out.println("Fallback: last modification date "+d);
				d = new Date(fInput.lastModified());
			} else {
				Directory dir = metadata.getFirstDirectoryOfType(Mp4Directory.class);
				d = dir.getDate(Mp4Directory.TAG_CREATION_TIME);
				
//				for (Directory directory : metadata.getDirectories()) {
//					for (Tag tag : directory.getTags()) {
//						System.out.format("[%s] - %s = %s\n", directory.getName(), tag.getTagName(), tag.getDescription());
//					}
//					if (directory.hasErrors()) {
//						for (String error : directory.getErrors()) {
//							System.err.format("ERROR: %s", error);
//						}
//					}
//				}
			}
		}
		if (DECALAGE_HORAIRE_VIDEO_IN_MS != 0) {
			// TODO
		}
		return d;
	}
	
	/**
	 * 
	 * @param fInput
	 * @return
	 */
	private static Date getDateFromImage(File fInput) {
		Metadata metadata = getMetaData(fInput);
		if (metadata != null) {
			if (LOG_EXIF) {
				metadata.getDirectories().forEach(new Consumer<Directory>() {
					  
					@Override
					public void accept(Directory d) { 
						System.out.println(d.toString());
						
						Collection<Tag> tags = d.getTags();
						tags.forEach(new Consumer<Tag>() {
	
							@Override
							public void accept(Tag t) { 
								// t.getTagName()+" "+
								System.out.println(t.toString());	
							}
						}); 
						System.out.println(" ");
					} 	
				});
			}
			
			Directory dir = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
			Date date = dir != null ? dir.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL) : null;
			if (date == null) {
				dir = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
				date = dir != null ? dir.getDate(ExifIFD0Directory.TAG_DATETIME) : null;
			}
			if (date != null) {
				if (DECALAGE_HORAIRE_PHOTO_IN_MS != 0) {
					date.setTime(date.getTime()+DECALAGE_HORAIRE_PHOTO_IN_MS);
				}
				return date;
			}
		}
		return null;
	}

	private static File getDestinationFolder(String stringifiedDate) {
		File dest;
		if (CREATE_DIRECTORY_PER_DAY) {
			dest = datedDirsHandler.getDatedDir(stringifiedDate);
		} else {
			dest = new File(DEST);
		}
		if (dest == null || dest.exists() != true) {
			System.err.println("Failed to open destination directory");
		}
		return dest;
	}
	
	/**
	 * 
	 * @param media
	 * @param date
	 */
	private static void performOnFile(File media, Date date) {
		String newDateString = destDateFormat.format(date);
		
		if (skipBefore != null && date.before(skipBefore)) {
			System.out.println("> Skipping - date "+newDateString+" - file: "+media.getName());
			return;
		}
		if (skipAfter != null && date.after(skipAfter)) {
			System.out.println("> Skipping - date "+newDateString+" - file: "+media.getName());
			return;
		}
		
		String newHourString = destHourFormat.format(date);
		String newDateHourString = newDateString + "_" + newHourString;

		if (media.getName().indexOf(newDateHourString) != -1) {
			// Unnecessary update
			System.out.println("Skipping "+media.getPath());
		} else {
			Matcher m = p.matcher(media.getName());
			String fileName;
			if (m.matches()) {
				fileName = m.group(1);
			} else {
				fileName = media.getName();
			}
				
			File destFile = new File(getDestinationFolder(newDateString), newDateHourString + (id != null ? "_"+id : "") + "_" + fileName);
			

			if (PERFORM_RSYNC) {
				// rsync -tvn "$@" $destination
				String command = "rsync -tvv \""+media.getPath()+"\" \""+destFile.getPath()+"\"";
				System.out.println(command);
				String s;
				Process p;
				
				BufferedReader br = null;
				InputStreamReader isr = null;
				try {
					p = Runtime.getRuntime().exec(command);
					isr = new InputStreamReader(p.getInputStream());
					br = new BufferedReader(isr);
					
					while ((s = br.readLine()) != null) {
						System.out.println(s);
					}
					p.waitFor();
					//System.out.println ("exit: " + p.exitValue());
					p.destroy();
				}
				catch (Exception e) {
					System.err.println("Failed to execute command");
					e.printStackTrace();
				}
				finally {
					if (br != null) {
						try { br.close(); } catch(Exception e) {}
					}
					if (isr != null) {
						try { isr.close(); } catch(Exception e) {}
					}
				}
				
				//System.exit(0);
				
			} else if (destFile.exists()) {
				System.out.println(" > skipping - destination already exists: "+destFile.getPath());

			} else if (SIMULATION) {
				System.out.println("would "+(COPY_INSTEAD_OF_RENAME ? "copy " : "rename ")+media.getName()+" to "+destFile.getPath());

			} else if (COPY_INSTEAD_OF_RENAME) {
				// Copy
				try {
					System.out.println("Copying "+media.getName()+" to "+destFile.getPath());
					FileUtils.copyFile(media, destFile);
					// System.out.println(" > Successfully copied "+destFile.getPath());
				} catch (IOException e) {
					System.err.println("Failed to copy  "+media.getPath()+"  to  "+destFile.getPath());
					e.printStackTrace();
				}
				
			} else {
				// Rename
				media.renameTo(destFile);
				System.out.println("Renamed "+media.getPath()+"  to  "+destFile.getPath());
			}
		}
	}

}
