package fr.guirip;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

public class TestClass {

	public static void main(String[] args) {
		testDateFormat();

	}
	
	private static void showProperties(){
		Properties props = System.getProperties();
		
		Iterator<Object> it = props.keySet().iterator();
		while (it.hasNext()){
			String key = (String) it.next();
			System.out.println( key + " : " + System.getProperty( key ));
		}
	}
	
	private static void testDateFormat() {
		//String date = "Tue Jul 09 12:36:09 CEST 2019";
		
		//Pattern p = Pattern.compile("([A-Z][a-z]{2}) ([A-Z][a-z][a-z]) (\\d{2}) ");
		
		
		
		final SimpleDateFormat df = new SimpleDateFormat("EEE MMM d HH':'mm':'ss' CEST 'yyyy");

		
		try {
			System.out.println( new Date(df.parse("Tue Jul 09 12:36:09 CEST 2019").getTime()) );
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		/*final SimpleDateFormat df = new SimpleDateFormat("M d HH':'mm':'ss' CEST 2019'");

		
		try {
			System.out.println( df.parse("7 09 12:36:09 CEST 2019") );
		} catch (ParseException e) {
			e.printStackTrace();
		}*/
	}
}
